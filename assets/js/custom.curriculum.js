jQuery( document ).ready( function( $ ){

	// Intro size setup


	setupIntro(); // On ready, resize intro

	$( window ).resize( function() { setupIntro(); } ); // On resize

	function setupIntro() {
		var infoBox = $('.info' );
		var titleBox = $( '.info').find('.container');
		$( '.info' ).find( '.container' ).css( 'top', ( ( infoBox.height() - titleBox.height() - 20 ) / 2) + 'px' );
	}

	// Making navigation sticky on scroll

	$( '#nav' ).sticky( { topSpacing:10 } );

	$( '#nav ul' ).onePageNav( { scrollSpeed: 400 } );

	$(window).scroll(function(){
		if($(window).scrollTop() < $(window).height()/2) {
			$('#nav').find('li').removeClass('current');
		}
	});

// Jump to various links in page. I know this is disgusting, and there must be better ways.
// I just don't have time to find them.

$("#scratch-did-link").click(function() {
   $(window).scrollTo(document.getElementById("scratch-did"),800,{margin:true});
});

$("#legohouse-did-link").click(function() {
   $(window).scrollTo(document.getElementById("legohouse-did"),800,{margin:true});
});

$("#ideastudio-did-link").click(function() {
   $(window).scrollTo(document.getElementById("ideastudio-did"),800,{margin:true});
});

$(".skyparade-did-link").click(function() {
   $(window).scrollTo(document.getElementById("skyparade-did"),800,{margin:true});
});


// When it's not the latest blog post, it should go to id: principles-open-ended-thought
$(".principles-open-ended-thought-link").click(function() {
   $(window).scrollTo(document.getElementById("thought"),800,{margin:true});
});

$( '.fx-backstretch' ).find( '.info' ).backstretch('assets/img/Mitsubishi_fukagawa-Kawase_Hasui.jpg');  



});
