from ina219 import INA219, DeviceRangeError
from time import sleep
from sys import argv


SHUNT_OHMS = 0.1
MAX_EXPECTED_AMPS = 1.0
ina = INA219(SHUNT_OHMS, MAX_EXPECTED_AMPS)
ina.configure(ina.RANGE_16V)

def read_ina219(argument):
    try:
        if argument == 'voltage':
            print('{0:0.2f} V'.format(ina.voltage()))
	elif argument == 'current':
            print('{0:0.2f} mA'.format(ina.current()))
        elif argument == 'power':
            print('{0:0.2f} mW'.format(ina.power()))
        elif argument == 'shunt_voltage':
            print('{0:0.2f} mV'.format(ina.shunt_voltage()))

    except DeviceRangeError as e:
        # Current out of device range with specified shunt resister
        print(e)

read_ina219(argv[1])
