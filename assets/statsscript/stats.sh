#From https://github.com/lowtechmag/materialserver



#!/bin/bash
cd "$(dirname "$0")"


precision=2 #how many decimal places 
units=1000000 #for W/V/A use 1000 for mW/mV/mA


#Battery Measurements

bat_power="`python readina219.py power`"
bat_current="`python readina219.py current`"
bat_voltage="`python readina219.py voltage`"


#Other statistics

upt="`uptime -p`"

time="`date +'%H:%M %Z'`"

`python3 darksky.py`
today=(`date +"%F"`)
weather=$( cat forecast-${today} )

#prepare and return the json
#stats="ac_power|ac_current|ac_voltage|ac_used|bat_power|bat_current|bat_voltage|bat_capacity|bat_charging|temperature|uptime|load_1|load_5|load_15|local_time|today|tomorrow|day_after_t|today_icon|tomorrow_icon|day_after_t_icon
#$ac|$bat|$temp|$upt|$loads|$time|$weather"


stats="uptime|local_time|bat_power|bat_current|bat_voltage|today|tomorrow|day_after_t|today_icon|tomorrow_icon|day_after_t_icon
$upt|$time|$bat_power|$bat_current|$bat_voltage|$weather"

jq -Rn '
( input  | split("|") ) as $keys |
( inputs | split("|") ) as $vals |
[[$keys, $vals] | transpose[] | {key:.[0],value:.[1]}] | from_entries
' <<<"$stats"
